import axios from '../utils/http'
import type { CommunityData } from '~/types/text'

// 获取用户社区号
export const fetchGetCommunity = () => {
  // 返回的数据格式可以和服务端约定
  return axios.get<CommunityData>('/api/Admin/GetCommunityInfo?communityID=60066')
}
