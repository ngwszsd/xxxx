// 获取社区信息
export interface CommunityData {
    CommunityID?: number
    CommunityName?: string
    DunsCode?: string
    RegistYear?: number
    T1NodeID?: number
    T1002LeafID?: number
    LoginDivTop?: number
    LoginDivLeft?: number
}