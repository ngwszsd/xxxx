import { Breadcrumb, BreadcrumbItem, Button } from "ant-design-vue";
import { defineComponent, onMounted, withModifiers } from "vue";
import { fetchGetCommunity } from "~/services/text";
import { useCounterStore } from '~/store/counter'
const Text = defineComponent({
  setup() {
    const count = ref(0);
    const counter = useCounterStore();
    const inc = () => {
      count.value++;
    };

    /**
     *
     */
    const getCommunity = async () => {
      const res = await fetchGetCommunity();
    };
    // onMounted(() => {
    //   getCommunity();
    // });
    return () => (
      <Breadcrumb>
        <BreadcrumbItem>Home</BreadcrumbItem>
        <BreadcrumbItem>
          <a onClick={getCommunity}>Application Center</a>
        </BreadcrumbItem>
        <BreadcrumbItem>
          <a onClick={inc}>111{{ count }}</a>
        </BreadcrumbItem>
        <BreadcrumbItem>An Application</BreadcrumbItem>家中有事，请假三天
      </Breadcrumb>
    );
  },
});

export default Text;
